<?php
/**
 * @package Nextapp
 */
/*
Plugin Name: Nextapp
Description: Nextapp interface is blogging web site to provide mobile phone client used to access the blog classification, the article and comment data interface.
Version: 1.0.5
Author: Daye
License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/

define('NEXTAPP_VERSION', '1.0.5');
define('NEXTAPP_PATH', dirname(__FILE__));

require NEXTAPP_PATH . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'Nextapp.php';
if (is_admin()) {
	function nextapp_menu() {
		if (function_exists('add_plugins_page')) {
			add_plugins_page('Nextapp Manage', Nextapp::__('Nextapp Settings'), 'edit_plugins', 'nextapp', 'nextapp_options');
		}else {
			add_pages_page('Nextapp Manage', Nextapp::__('Nextapp Settings'), 'edit_plugins', 'nextapp', 'nextapp_options');
		}
	}
	function nextapp_options() {
		if (!current_user_can('edit_plugins')) {
			wp_die(__('You do not have sufficient permissions to access this page.'));
		}
		include NEXTAPP_PATH . DIRECTORY_SEPARATOR . 'adminhtml' . DIRECTORY_SEPARATOR . 'render.phtml';
	}
	add_action('admin_menu', 'nextapp_menu');
	
	# before save options.
	function nextapp_whitelist_options($whitelist_options) {
		return array_merge($whitelist_options, array(
			'nextapp'	=> array_keys(Nextapp::options())
		));
	}
	add_filter('whitelist_options', 'nextapp_whitelist_options');
}else {
	Nextapp::singleton()->run();
}
